import ipaddress
from datetime import datetime

from flask import Flask, render_template, request

app = Flask(__name__, static_url_path='/assets')


@app.route('/', methods=['GET', 'POST'])
def index():
    if request.method == 'POST':
        ip = request.form['ip']
    else:
        ip = '127.0.0.1'
    try:
        ip_obj = ipaddress.IPv4Address(ip)
        result = 'Date for this IP: ' + str(datetime.fromtimestamp(int.from_bytes(ip_obj.packed, 'big')))
    except Exception as e:
        result = 'Parsing error: ' + str(e)
        app.logger.error('Problems for ip "' + ip + '": ' + str(e))
    return render_template('index.html', ip=ip, result=result)
