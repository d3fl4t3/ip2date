FROM python:3-onbuild
COPY . /usr/src/app
EXPOSE 8000
ENTRYPOINT ["python3", "run.py"]
